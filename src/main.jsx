import React from 'react'
import ReactDOM from 'react-dom/client'
import {Countdown} from "./components/Countdown";
import './index.css'
import Video from './resources/Northern Lights - 2267.mp4';

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <div className="container">
            <h1 className="title">Friday is <span>coming</span></h1>
            <Countdown/>
            <footer>
                <a href="https://gitlab.com/react-public-samples/react-countdown" target="_blank">
                    <img
                        src="https://gitlab.com/assets/favicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png"
                        alt="gitlab"/>
                </a>

            </footer>
        </div>
        <video src={Video} muted loop autoPlay/>
    </React.StrictMode>
)
