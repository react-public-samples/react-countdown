
export const padLeftZero = (value) => {
    if(value.toString().length < 2){
        return `0${value}`;
    }

    return value;
}