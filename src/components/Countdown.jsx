import {useEffect, useState} from 'react'
import {padLeftZero} from "../util/util";
import './Countdown.css'

const date = new Date();
const nextFriday = new Date(date.setDate(
        date.getDate() + ((7 - date.getDay() + 5) % 7 || 7),
    ),
)

const defaultCountdown = {
    days: "00",
    hours: "00",
    minutes: "00",
    seconds: "00"
}

export const Countdown = () => {

    const [countdown, setCountdown] = useState(defaultCountdown);

    const today = new Date();
    const diff = (nextFriday - today) / 1000;

    useEffect(() => {
        const interval = setInterval(updateCountdown, 1000);
        return () => clearInterval(interval);
    }, [diff]);

    const updateCountdown = () => {
        setCountdown({
            days: padLeftZero(Math.floor(diff / 3600 / 24)),
            hours: padLeftZero(Math.floor(diff / 3600) % 24),
            minutes: padLeftZero(Math.floor(diff / 60) % 60),
            seconds: padLeftZero(Math.floor(diff) % 60)
        });
    };


    return (
        <>
            <div className="count-container">
                <div className="time">
                    <p>{countdown.days}</p>
                    <p>Days</p>
                </div>
                <span>:</span>
                <div className="time">
                    <p>{countdown.hours}</p>
                    <p>Hours</p>
                </div>
                <span>:</span>
                <div className="time">
                    <p>{countdown.minutes}</p>
                    <p>Minutes</p>
                </div>
                <span>:</span>
                <div className="time">
                    <p>{countdown.seconds}</p>
                    <p>Seconds</p>
                </div>
            </div>
        </>
    );


};